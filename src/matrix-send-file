#!/usr/bin/env sh

#  Post a message to a given Matrix Room
#  Copyright (C) 2019 Darshit Shah <git@darnir.net>
#
# This file is part of the Matrix Shell Suite
#
# Matrix Shell Suite is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Matrix Shell Suite is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Matrix Shell Suite.  If not, see <http://www.gnu.org/licenses/>.

## @file matrix-send-file
## @brief Send a file to a Matrix room
##
## \param[in] room_id. The Matrix internal room id
## \param[in] file. Path to file that should be sent
## \param[in] cookie_file. Path to matrix-cookie file (optional)
## \return The logged event details
##
## Sends a file to a Matrix chat room.
##
## The \p room_id is the Matrix Room ID as specified by
## [Section 4.2.2](https://matrix.org/docs/spec/appendices.html#id15).
## It can be found by looking through the graphical clients, or using the
## `matrix-joined-rooms` script provided in this suite.
##
## \p file is the path to a file that exists on the system which should be
## sent to the room at \p room_id. This script will attempt to automatically
## detect the mimetype of the file and report it accordingly to the server.
##
## According to the CS API, this script will first upload the file to the
## matrix content repository of the user's homeserver and then send a message
## to the room containing details about the uploaded file.
##
## Example:
## ```
## $ matrix-post-message '!lUwVj8WvimcSdHlGzd:example.matrix.org' ./file.pdf
## {"event_id": "$1528213310846yoVKm:example.matrix.org"}
## ```
##
## @author Darshit Shah <git@darnir.net>
## @copyright GNU GPLv3+

set -e
set -u

script_dir=$(CDPATH='' cd -- "$(dirname -- "$0")" && pwd -P)

#shellcheck source=./src/utils/optparse.sh
. "${script_dir}/utils/optparse.sh"
#shellcheck source=./src/utils/functions.sh
. "${script_dir}/utils/functions.sh"
#shellcheck source=./src/utils/cookie.sh
. "${script_dir}/utils/cookie.sh"

if [ $# -gt 3 ] || [ $# -lt 2 ]; then
	>&2 echo "Usage: $0 <room_id> <file> [cookie_file]"
	exit 1
fi

exists jq

room_id="$1" && shift
file="$1" && shift
cookie_file=${1:-$MATRIX_COOKIE}

log_debug "Room ID:     $room_id"
log_debug "Filepath:    $file"
log_debug "cookie-file: $cookie_file"

verify_cookie_exists "$cookie_file"

if [ ! -f "$file" ]; then
	>&2 echo "File $file not found. Cannot upload"
	exit 2
fi

auth_token="$(get_auth_token "$cookie_file")"
homeserver="$(get_homeserver_base "$cookie_file")"

mime_type=$(get_mime_type "$file")
# Use wc -c to get the file size. It seems like the safest, most portable way
# to do this
file_size=$(wc -c < "$file")
log_debug "File: $file ($mime_type : $file_size)"

clean_filename=$(basename "$file" | jq -rRs '@uri')
REST_ENDPOINT="_matrix/media/r0/upload?filename=${clean_filename}"
HTTP_ENDPOINT="$homeserver/${REST_ENDPOINT}"

server_response=$(HPOST_FILE "$HTTP_ENDPOINT" "$auth_token" "$file") || {
	errcode="$(echo "$server_response" | jq '.errcode')"
	msg="$(echo "$server_response" | jq '.error')"

	if [ "$errcode" != 'null' ]; then
		log_error "$msg ($errcode)"
	else
		log_error "Unknown Error. Increase logging verbosity for more information"
	fi
	exit 1
}

echo "$server_response"

media_uri="$(echo "$server_response" | jq ".content_uri" | tr -d \")"
if [ x"$media_uri" = x'null' ]; then
	log_error "content_uri not found in $media_uri"
	exit 1
fi

txnID="$(echo "$(date +%s)$PPID" | md5sum | awk '{print $1}')"

REST_ENDPOINT="_matrix/client/r0/rooms/${room_id}/send/m.room.message/${txnID}"
HTTP_ENDPOINT="$homeserver/${REST_ENDPOINT}"

#shellcheck disable=SC2016
msg_body="$(printf '{"msgtype":"m.file", "body":"%s", "url":"%s", "info":{"mimetype": "%s", "size": %d}}' \
	"$(basename "$file" | sed 's/"/\\"/g')" \
	"$media_uri" \
	"$mime_type" \
	"$file_size" \
	)"

log_debug "Message Body: $(echo "$msg_body" | jq .)"

HPUT_AUTH "${HTTP_ENDPOINT}" "${auth_token}" "$msg_body"
