#!/usr/bin/env sh

#  Post a message to a given Matrix Room
#  Copyright (C) 2019 Darshit Shah <git@darnir.net>
#
# This file is part of the Matrix Shell Suite
#
# Matrix Shell Suite is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Matrix Shell Suite is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Matrix Shell Suite.  If not, see <http://www.gnu.org/licenses/>.

## @file matrix-post-message
## @brief Post a message to a Matrix room
##
## \param[in] room_id. The Matrix internal room id
## \param[in] cookie_file. Path to matrix-cookie file (optional)
## \return The logged event details
##
## Sends a message to a Matrix chat room.
##
## The \p room_id is the Matrix Room ID as specified by
## [Section 4.2.2](https://matrix.org/docs/spec/appendices.html#id15).
## It can be found by looking through the graphical clients, or using the
## `matrix-joined-rooms` script provided in this suite.
##
## This script reads the message to send to the room over `stdin`.
## Earlier it used to accept the message as a CLI parameter, but having the
## message visible in `ps` and the shell history isn't is great idea.
## One can still emulate the old process using Bash herestrings.
##
## Example:
## ```
## $ matrix-post-message '!lUwVj8WvimcSdHlGzd:example.matrix.org'
## > Test Message
## {"event_id": "$1528213310846yoVKm:example.matrix.org"}
## ```
##
## @author Darshit Shah <git@darnir.net>
## @copyright GNU GPLv3+

set -e
set -u

script_dir=$(CDPATH='' cd -- "$(dirname -- "$0")" && pwd -P)

#shellcheck source=./src/utils/optparse.sh
. "${script_dir}/utils/optparse.sh"
#shellcheck source=./src/utils/functions.sh
. "${script_dir}/utils/functions.sh"
#shellcheck source=./src/utils/cookie.sh
. "${script_dir}/utils/cookie.sh"

if [ $# -gt 2 ] || [ $# -lt 1 ]; then
	>&2 echo "Usage: $0 <room_id> [cookie_file]"
	exit 1
fi

exists jq

room_id="$1"

cookie_file=${2:-$MATRIX_COOKIE}
verify_cookie_exists "$cookie_file"

read -r message

auth_token="$(get_auth_token "$cookie_file")"
homeserver="$(get_homeserver_base "$cookie_file")"

txnID="$(echo "$(date +%s)$PPID" | md5sum | awk '{print $1}')"

REST_ENDPOINT="_matrix/client/r0/rooms/${room_id}/send/m.room.message/${txnID}"
HTTP_ENDPOINT="$homeserver/${REST_ENDPOINT}"

#shellcheck disable=SC2016
msg_body="$(printf '{"msgtype":"m.text", "body":"%s"}' "$message")"

HPUT_AUTH "${HTTP_ENDPOINT}" "${auth_token}" "$msg_body"
