#!/usr/bin/env sh

#  Cookie file handling for Matrix Shell Suite
#  Copyright (C) 2019 Darshit Shah <git@darnir.net>
#
# This file is part of the Matrix Shell Suite
#
# Matrix Shell Suite is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Matrix Shell Suite is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Matrix Shell Suite.  If not, see <http://www.gnu.org/licenses/>.

## @file cookie.sh
## @brief Helper functions for dealing with matrix-cookies
##
## @author Darshit Shah <git@darnir.net>
## @copyright GNU GPLv3+

data_dir="${XDG_DATA_HOME:-$HOME/.local/share}/mss"

# Make the data dir on source if it doesn't exist
if [ ! -d "$data_dir" ]; then
	mkdir -p "$data_dir"
fi

## @var String MATRIX_COOKIE
## Path to cookie file.
##
## The `MATRIX_COOKIE` contains information about the active login as returned
## by the homeserver. It is a json file containing the user-id, a valid access
## token, path to the homeserver and the device id for the current login. An
## example cookie looks like:
## ```json
## {
##   "user_id": "@testuser:example.matrix.org",
##   "access_token": "<an access_token>",
##   "home_server": "https://example.matrix.org",
##   "device_id": "HGDC8GH",
## }
## ```
##
## **IMPORTANT**: You must take care to secure the cookie file on your own. It
## _does not_ contain your password, however it _does_ contain an access token
## for your account on a Matrix homeserver in plainext. Anyone with access to
## the cookie can impersonate you on the homeserver.
##
## The default location of this file is determined according to the XDG Base
## Directory Specification.
## If `$XDG_DATA_HOME` is set, then that is used as the base directory for MSS.
## Else, it falls back to the default of `$HOME/.local/share`. In the base
## directory, a holder directory called `mss` is created which contains the
## cookie file.
export MATRIX_COOKIE="${data_dir}/matrix.cookie"

verify_cookie_exists() {
	if [ ! -f "$1" ]; then
		>&2 printf "No Matrix Cookie found at %s\n" "$1"
		exit 2
	fi
}

get_auth_token() {
	jq '.access_token' "$1" | tr -d '"'
}

get_homeserver_base() {
	# This is a hack since I can't figure out how to let jq parse a kayname
	# containing a '.'
	jq '.well_known | values[keys[0]] | .base_url' "$1" | tr -d '"' | sed 's#/$##g'
}

get_user_id() {
	jq '.user_id' "$1" | tr -d '"'
}
