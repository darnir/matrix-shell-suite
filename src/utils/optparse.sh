#!/usr/bin/env sh

# shellcheck disable=SC2154
if [ -z "${script_dir}" ]; then
	>&2 echo "ERROR: Script sourced without setting \$script_dir"
fi

_CONF_FILE="${XDG_CONFIG_HOME:-$HOME/.config}/mss/conf.sh"
if [ -e "${_CONF_FILE}" ]; then
	#shellcheck disable=SC1090
	. "${_CONF_FILE}"
fi

# shellcheck source=./src/HTTP/wget.sh
. "${script_dir}/HTTP/${HTTP_CLIENT:-wget}.sh" || { echo "$HTTP_CLIENT client conf not found" && exit 1; }
